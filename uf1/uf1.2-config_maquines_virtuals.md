# UF1.2 Configuració de màquines virtuals

Les màquines virtuals ens permetran tenir diferents sistemes operatius executant-se sobre el mateix maquinari.

## 2.1 Màquina real (S.O. hoste) i màquina virtual (S.O. convidat)

- Màquina real: És el maquinari físic que tenim, amb tots els dispositius físics (cpu, memòria, interfícies...)
- Sistema operatiu hoste (host): És el sistema operatiu que tindrem instal.lat al maquinari real i que ens permetrà executar aplicacions i processos. És l'encarregat de gestionar la màquina real.
- Màquina virtual: Es tracta d'un programari (aplicació) que té com a finalitat simular l'existència de tot un maquinari programat. Per tant aquest maquinari és com el maquinari físic però està programat com una aplicació. Això ens permetrà instal.lar-hi un nou sistema operatiu.
- Sistema operatiu convidat (guest): És un sistema operatiu com qualsevol altre però que hem instal.lat en l'aplicació de virtualització que ens ha simulat una màquina real. Per tant aquest sistema operatiu convidat s'executa com una aplicació més per al sistema operatiu hoste.

## 2.2 Virtualització

Existeixen diversos sistemes de virtualització (programaris que permeten simular un maquinari, que serà virtual i que anomenarem hypervisors):

- [LXC](https://en.wikipedia.org/wiki/LXC) – lightweight Linux container system
- [OpenVZ](https://en.wikipedia.org/wiki/OpenVZ) – lightweight Linux container system
- [Kernel-based Virtual Machine](https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine)/[QEMU](https://en.wikipedia.org/wiki/QEMU) (KVM) – open-source hypervisor for Linux and SmartOS
- [Xen](https://en.wikipedia.org/wiki/Xen) – Bare-Metal hypervisor
- [User-mode Linux](https://en.wikipedia.org/wiki/User-mode_Linux) (UML) paravirtualized kernel
- [VMware ESXi](https://en.wikipedia.org/wiki/VMware_ESXi) and GSX – hypervisors for Intel hardware
- [Hyper-V](https://en.wikipedia.org/wiki/Hyper-V) – hypervisor for Windows by Microsoft
- [PowerVM](https://en.wikipedia.org/wiki/PowerVM) – hypervisor by IBM for [AIX](https://en.wikipedia.org/wiki/IBM_AIX), Linux and IBM i
- [Parallels Workstation](https://en.wikipedia.org/wiki/Parallels_Workstation) – hypervisor for Mac by Parallels IP Holdings GmbH
- [Bhyve](https://en.wikipedia.org/wiki/Bhyve) – hypervisor for [FreeBSD](https://en.wikipedia.org/wiki/FreeBSD) 10

Aquests hypervisors porten associat algun tipus de programari que permet gestionar la creació i events associats a les màquines virtuals:

- libvirt + virt-manager: És una de les solucions que permet gestionar-los tots des de GUI
- libvirt + virsh: El mateix que virt-manager però des de CLI
- oVirt: Interfície web per gestionar màquines virtuals en hypervisors KVM/qemu
- VMware Workstation i VMware Player: Gestió d'hypervisors VMware
- VirtualBox: Gestió d'hypervisors Oracle
- IsardVDI: Gestió centralitzada d'hipervisors KVM/qemu
- Proxmox: Gestió de contenidors lleugers (LXC, OpenVZ) i d'hypervisors KVM/qemu
- ...

## 2.3 Creació de màquines virtuals

Per a crear màquines virtuals farem ús de KVM/qemu ja que és programari lliure i gratuït, basant-nos en una distribució RedHat/Fedora/CentOS com a sistema operatiu hoste.

```
COMPROVACIÓ PRÈVIA
Cal que el sistema operatiu hoste tingui accés a la virtualització de maquinari. Aquesta opció depèn del processador físic ( Intel VT o AMD-V) que tingui la màquina real i habitualment es pot activar aquesta funcionalitat de virtualització des de la BIOS.

Amb la comanda: 

egrep '(vmx|svm)' /proc/cpuinfo

podem comprovar si el nostre processador permet aquesta funcionalitat de virtualització i es troba activada a la BIOS. Veurem una sortida com la següent en cas que la tingui activa:

flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl **vmx** **smx** est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 popcnt aes lahf_lm kaiser tpr_shadow vnmi flexpriority ept vpid dtherm ida arat

```

### 2.3.1 Creació manual d'una màquina virtual (qemu)

L'arrencada d'una màquina virtual es farà amb l'hypervisor qemu que haurem de tenir instal.lat. Primer de tot arrencarem una senzilla màquina des de línia de comandes.

1. **Paquets a instal.lar**: dnf install qemu-kvm
2. **Descarregar una màquina virtual de prova**: Anirem al web http://www.qemu-advent-calendar.org/2016/ on hi trobem divertides màquines antigues ja instal.lades. Si cliquem a la porta nº10 hi trobem el 'Epic Pinball Demo' que descarregarem. Dins hi trobarem:
   1. freedos2016.qcow2: Aquest és el disc dur que volem arrencar amb la nostra màquina virtual. Les màquines virtuals veuen els discs durs com a reals però en realitat no són més que fitxers com aquests d'on llegeixen i hi escriuen les dades.
   2. run.sh: Codi bash que en executar-se ens crearà i arrencarà un procés qemu amb aquesta màquina virtual.
3. **Arrencar la màquina virtual**: Podriem fer servir el run.sh però ho farem amb la comanda que realment fa servir el run.sh: **qemu-system-x86_64 freedos2016.qcow2 -soundhw sb16 \
   ​    -vga std,retrace=precise -display sdl** on si ho desglossem veurem que:
   - qemu-system-x86_64: És l'hipervisor que simularà processadors i arquitectures de 64 bits (compatibles amb les de 32 bits).
   - freedos2016.qcow2: Aquest primer paràmetre li diu quin és el fitxer que ha de pendre com a disc la màquina virtual que crearem.
   - soundhw sb16: Simulem una tarja de so model soundblaster16 (de l'època del joc que volem arrencar)
   - vga std,...: Simulem una tarja gràfica estàndard vga (resolució màxima de 640x480 de l'època del joc)
   - display sdl: Simulem una pantalla genèrica
4. **Juguem-hi!**: Si ho hem fet bé ara se'ns obrirà una finestra (pantalla) de la màquina virtual que acabem d'arrencar i començarà el joc de pinball. En realitat es tracta d'un joc fet per al sistema operatiu MS-DOS que és realment el sistema operatiu que estem arrencant.

Ara que hem vist com crear una màquina amb hipervisor qemu podeu explorar les diferents opcions de processador, pantalla, so, etc... que ens permetrà simular:

- man qemu-system-x86_64
- qemu-system-x86_64 -cpu ?
- qemu-system-x86_64 -soundhw ?
- ...

### 2.3.2 Creació GUI d'una màquina virtual (virt-manager)

Per poder crear màquines qemu amb la GUI del virt-manager ens caldrà instal.lar:

- virt-manager: Aplicació gràfica que ens permetrà gestionar les màquines.
- libvirtd: Servei que controlarà l'hipervisor KVM/qemu de la nostra màquina

Per a fer-ho executarem: **dnf install @Virtualization**. Aquesta comanda ens instal.larà en realitat tots els paquets necessaris per tal de tenir un complert sistema de virtualització KVM/qemu.

1. **Obrim el virt-manager**: Ens demanarà la clau de root ja que el servei libvirtd corre sota l'usuari root. Si no la sabem sempre podem executar-lo amb 'sudo virt-manager' i posar la nostra clau (hem de tenir permisos, clar). Si ens diu que no pot obrir el 'display' hem d'executar prèviament al compte de nostre usuari la comanda: xhost +

   Una vegada obert, si tot ha anat bé, veurem el nostre hipervisor QEMU/KVM connectat.

   **Crear nova màquina**: 

   1. Triar com volem instal.lar el sistema operatiu: Des de ISO, per xarxa PXE o fent servir una imatge de disc (un fitxer). Triarem aquesta última opció per tal de crear la màquina del pinball anterior, ara de manera gràfica

   2. Triar emmagatzematge i sistema operatiu: Al clicar a navegar (browse) podrem bucar on es troba el disc freedos2016.qcow2. Com que per defecte usa com a directori pels discs /var/lib/libvirt/images haurem d'afegir una nova reserva (pool) amb el signe + que hi ha a baix a l'esquerra. Seleccionarem el directori on hi tenim el nostre disc (ex: /home/usuari/Baixades) i li donarem un nom a aquest directori. Ara que ja el tenim veurem a la part dreta el disc que seleccionarem.

      Al tipus de sistema operatiu seleccionarem 'Altres' i a versió 'MS-DOS 6.22'

   3. Ajustos de memòria i processador: Ajusteu segons convingui. Per a la nostra màquina amb 2MB de RAM és suficient (penseu que és d'una època on això teníen els ordinadors) i evidentment no sabrà que fer-ne de més d'un processador.

   4. Finalitzem: Li posem nom. Aquí també podríem personalitzar el maquinari que volem simular (i que, segons el que hem respost ha seleccionat el virt-manager), però provarem d'iniciar-la prement 'Finalitzar'.

      Segurament no iniciarà (no es veurà res). Això es degut a que le maquinari seleccionat no es correspon amb el que aquest sistema operatiu és capaç de detectar i espera trobar-se. Concretament haureu d'aturar la màquina i anar a la configuració de maquinari (botó bombeta). Allí veureu que la tarja d'àudio que s'ha seleccionat és ICH6 quan, si recordeu, abans en arrencar-la amb CLI hem vist que esperava trobar-se una SoundBlaster16 (sb16). Canvieu-la i ja la podreu arrencar.

      L'altre problemàtica que us podeu trobar és que ara vaigi molt més ràpida. Segurament és pel tipus de processador que li està simulant. Haurem de jugar amb els diferents models per tal d'ajustar-ho.

### 2.3.3 Gestió de màquines virtuals en CLI (utilitats)

Les màquines virtuals que creeu amb el virt-manager en realitat són definicions en un fitxer XML que gestiona el servei libvirt. A aquest servei també hi podem accedir via CLI amb la comanda virsh.

- **virsh list**: Mostrarà les màquines arrencades. Amb el paràmetre **--all** ens mostrarà totes, també les apagades.
- **virsh start <id | nom>**: Indicant el nºid de màquina o el seu nom la podrem arrencar.
- **virsh destroy <id | nom>**: Per aturar-la
- **virsh undefine <id | nom>**: Amb aquesta comanda eliminarem la màquina (no els discs que tingui)
- **virsh dumpxml <id | nom>**: Ens volcarà per pantalla la definició xml de la màquina. Preneu-vos un moment en mirar per sobre el contingut. Hi veureu definicions de memòria, cpu, àudio... Podem fer-nos una còpia de seguretat de la configuració de maquinari de la màquina virtual si ho reenviem a un fitxer, per exemple afegint **> nom.xml**
- **virsh define <nom.xml>**: Ens crearà una nova màquina virtual amb el maquinari que hi hagi definit al fitxer nom.xml
- **virsh domdisplay <id | nom>**: Ens mostrarà la URI de conexio al visor. Amb l'ordre **virt-viewer <URI>**  que ens ha donat se'ns obrirà el visor a la màquina virtual.

Investigueu altres opcions del virsh amb el seu manual (man).

### 2.3.4 Gestió de discos virtuals (qemu-img)

Podem gestionar els discos amb l'ordre qemu-img:

- **qemu-img info <disc.qcow2>**: Ens proporcionarà informació del disc com el tamany real, tamany virtual, format... El tamany virtual és el màxim permès, el real és el que realment està ocupat pel sistema operatiu que hi ha dins.
- **qemu-img create -f <format> <disc.qcow2> <tamany>**: Amb aquesta comanda podem crear un fitxer anomenat disc.qcow2 que tingui un determinat format i tamany:
  - format: qcow2, raw...
  - tamany: En format 20G per exemple indicaria que el volem de 20GB virtuals màxims.

Busqueu també al manual del qem-img els diferents formats admesos.

### 2.3.5 Interfícies virtuals (virtio)

Tant els discos com la xarxa poden fer ús de maquinari simulat. Per exemple coneixem que pels discs existeixen les interfícies IDE, SATA, SCSII... i per la xarxa podem simular diferents tipus de targetes com e2000, rtl8931... 

Simular aquesta interfície suposa un cost computacional elevat. És a dir, que el rendiment que obtindrem serà força dolent respecte al que obtindriem en els mateixos discs i xarxes en el maquinari real. És per això que existeix una interfície virtual anomenada **virtio** que permet assolir pràcticament els mateixos rendiments que si no estiguéssim virtualitzant el sistema. 

Evidentment cal que el sistema operatiu que fem servir disposi dels controladors (drivers) apropiats per a aquest tipus d'interfície. Podeu trobar més informació a https://spice-space.org

### 2.3.6 Tipus de xarxes

Les xarxes poden ser de diferents tipus. Cadascuna ens proporcionarà un tipus de conexió diferent que ens pot ser útil en diferents escenaris:

- NAT: Per defecte a les màquines ens crearà una xarxa d'aquest tipus. Es tracta d'una xarxa amb un rang d'adreces que gestionarà el libvirt (mitjançant un servei anomenat dnsmasq) i que permetrà a la màquina comunicar-se amb altres ordinadors externs però no permetrà que altres ordinadors es comuniquin amb la nostra màquina virtual. La configuració és similar a la que tenim als routers de casa..
- ENCAMINADA (routed): En aquesta configuració la interfície de xarxa de la màquina virtual es connectada directament a la interfície real de l'ordinador. Per tant la seva configuració dependrà de la xarxa real, tal i com passaria amb qualsevol ordinador en aquella xarxa. Al virt-manager la veureu com a **dispositiu d'amfitrió**.
- AÎLLADA (isolated): Les màquines virtuals conectades a una xarxa aîllada només es podran veure entre si i amb la màquina hoste. Mai podran sortir cap a la xarxa real. Al virt-manager caldrà anar a les propietats de l'hipervisor per tal de crear xarxes aîllades que després podrem conectar a les nostres màquines virtuals.