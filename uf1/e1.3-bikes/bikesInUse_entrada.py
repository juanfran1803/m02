# coding=utf-8

# Farem ús de llibreries que contenen funcions que ens seran útils
# La llibreria requests ens permetrà conectar amb direccions URL
import requests

# Aquí executarem una petició a la API de bicing i guardarem les dades a
# la variable dades
dades = requests.get("https://www.bicing.cat/current-bikes-in-use.json").json()

# Ara la resposta del web està desada pel sistema operatiu a la memòria
# del sistema. És el sistema operatiu l'encarregat de gestionar en quin
# lloc dels GB de memòria RAM s'emmagatzema.

# Podem veure el contingut de la resposta emmagatzemada a la variable
# dades fent-ne un print
print(dades)
